package com.nt.courierdriver;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class OrderAssignmentNotification extends AppCompatActivity {

    MediaPlayer mMediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_assignment_notification);

        mMediaPlayer = MediaPlayer.create(this, R.raw.knock);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();

        Button goBackBtn = findViewById(R.id.go_back);
        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ORDERASS", "in here");
                finish();
            }
        });
        if(getIntent() != null){
         //get extras from getIntent
        }
    }

    @Override
    protected void onStop() {
        mMediaPlayer.release();
        super.onStop();

    }

    @Override
    protected void onPause() {
        mMediaPlayer.release();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaPlayer.start();
    }
}