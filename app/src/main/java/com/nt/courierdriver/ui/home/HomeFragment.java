package com.nt.courierdriver.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nt.courierdriver.OrderDetailActivity;
import com.nt.courierdriver.R;
import com.nt.courierdriver.data.ApiClient;
import com.nt.courierdriver.data.ApiInterface;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.data.LoginRepository;
import com.nt.courierdriver.data.model.DeliveryOrder;
import com.nt.courierdriver.data.model.DeliverySearchResponse;
import com.nt.courierdriver.data.model.HomeStats;
import com.nt.courierdriver.data.model.LoggedInUser;
import com.nt.courierdriver.data.model.LoginForm;
import com.nt.courierdriver.databinding.FragmentHomeBinding;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements IDeliveryOrderClickListener{

    private static final String TAG = "HomeActivity";
    private ApiInterface apiInterface;
    private ProgressBar mProgressBar;
    private FragmentHomeBinding mBinding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //final View root = inflater.inflate(R.layout.fragment_home, container, false);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        final View root = mBinding.getRoot();
        displayUserInformation(root);
        fetchPendingOrders(root);

        final SwipeRefreshLayout pullToRefresh = root.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchPendingOrders(root);
                pullToRefresh.setRefreshing(false);
            }
        });
        return root;
    }

    private void fetchStatsInfo() {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            SharedPreferences pref = this.getActivity().getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String token = pref.getString(Constants.PREF_USER_TOKEN, "");
            Call<HomeStats> call = apiInterface.doGetStats("Bearer "+token);
            call.enqueue(new Callback<HomeStats>() {
                @Override
                public void onResponse(Call<HomeStats> call, Response<HomeStats> response) {
                    Log.d(TAG,response.code()+"");
                    if(!response.isSuccessful()){
                        Log.d(TAG,response.message()+"");
                        return;
                    }

                    HomeStats searchResponse = response.body();
                    mBinding.setStats(searchResponse);

                }

                @Override
                public void onFailure(Call<HomeStats> call, Throwable t) {
                    showToast(R.string.fetch_user_stats_error);
                }
            });


        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void fetchPendingOrders(View root) {

        final RecyclerView orderRecyclerView = root.findViewById(R.id.order_recycler_view);
        mProgressBar = root.findViewById(R.id.progressBar2);
        mProgressBar.setVisibility(View.VISIBLE);
        //Creates an instance of the products adapter and initialized the click listener with `this`
        //Meaning it will use the listener implemented in this class.
        final OrderRecyclerAdapter orderRecyclerAdapter = new OrderRecyclerAdapter(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        orderRecyclerView.setLayoutManager(layoutManager);
        orderRecyclerAdapter.setDeliveryOrderList(Collections.EMPTY_LIST);
        orderRecyclerView.setAdapter(orderRecyclerAdapter);
        //Sets the data that will be displayed on the UI

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            SharedPreferences pref = this.getActivity().getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String token = pref.getString(Constants.PREF_USER_TOKEN, "");
            Call<DeliverySearchResponse> call = apiInterface.doGetDeliveryList(0, 10, "",true,"Bearer "+token);
            call.enqueue(new Callback<DeliverySearchResponse>() {
                @Override
                public void onResponse(Call<DeliverySearchResponse> call, Response<DeliverySearchResponse> response) {
                    Log.d("HomeActivity",response.code()+"");
                    mProgressBar.setVisibility(View.GONE);
                    if(!response.isSuccessful()){
                        Log.d("HomeActivity",response.message()+"");
                        return;
                    }

                    DeliverySearchResponse searchResponse = response.body();
                    List<DeliveryOrder> orders = searchResponse.getResults();
                    if(searchResponse.getTotal() == 0){
                        showToast(R.string.pending_delivery_order);
                    }
                  orderRecyclerAdapter.setDeliveryOrderList(orders);
                  orderRecyclerView.setAdapter(orderRecyclerAdapter);
                    fetchStatsInfo();

                }

                @Override
                public void onFailure(Call<DeliverySearchResponse> call, Throwable t) {
                    Log.d("HomeActivity", "in error "+t.getLocalizedMessage());
                    Log.d("HomeActivity", "in error "+t.getCause().getMessage());
                    mProgressBar.setVisibility(View.GONE);
                    showToast(R.string.fetch_delivery_order_error);
                }
            });


        }catch (Exception ex){
             ex.printStackTrace();
            Log.d("HomeActivity", "in error "+ex.getMessage());
        }

    }

    private void displayUserInformation(View root) {

        SharedPreferences pref = this.getActivity().getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
        String displayName = pref.getString(Constants.PREF_USER_DISPLAY_NAME, "");
        final TextView textView = root.findViewById(R.id.display_name);
        textView.setText(displayName);
    }

    @Override
    public void onClick(DeliveryOrder deliveryOrder) {
        Log.i("MainActivity", deliveryOrder.toString());
        Intent detailIntent = new Intent(getContext(), OrderDetailActivity.class);
        detailIntent.putExtra(Constants.Keys.DELIVERY_ORDER_DETAIL.toString(), deliveryOrder);
        startActivity(detailIntent);
    }

    private void showToast(@StringRes Integer errorString) {
        Toast.makeText(getContext(), errorString, Toast.LENGTH_LONG).show();
    }
}