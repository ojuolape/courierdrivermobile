package com.nt.courierdriver.ui.account;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.nt.courierdriver.R;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.ui.login.LoginActivity;

public class AccountFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_account, container, false);
        displayUserInformation(root);

        final Button logoutButton = root.findViewById(R.id.logout);
        final ProgressBar loadingProgressBar = root.findViewById(R.id.progressBar);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                removeUserInfoFromPref();
                Intent intent = new Intent(getContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().finish();
                startActivity(intent);
            }
        });
        return root;
    }

    private void displayUserInformation(View root) {

        SharedPreferences pref = this.getActivity().getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
        String displayName = pref.getString(Constants.PREF_USER_DISPLAY_NAME, "");
        String phoneNumber = pref.getString(Constants.PREF_USER_PHONE_NUMBER, "");
        final TextView textView = root.findViewById(R.id.account_display_name);
        textView.setText(displayName);
        final TextView phoneTextView = root.findViewById(R.id.account_phone_number);
        phoneTextView.setText(phoneNumber);
    }

    private void removeUserInfoFromPref(){
        SharedPreferences pref = getActivity().getSharedPreferences(Constants.PREF_USER,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor myEdit  = pref.edit();
        myEdit.putString( Constants.PREF_USER_DISPLAY_NAME, null);
        myEdit.putString( Constants.PREF_USER_NAME, null);
        myEdit.putString( Constants.PREF_USER_TOKEN, null);
        myEdit.commit();
    }
}