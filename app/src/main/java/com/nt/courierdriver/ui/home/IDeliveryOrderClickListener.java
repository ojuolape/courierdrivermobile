package com.nt.courierdriver.ui.home;

import com.nt.courierdriver.data.model.DeliveryOrder;

public interface IDeliveryOrderClickListener {
    void onClick(DeliveryOrder deliveryOrder);
}
