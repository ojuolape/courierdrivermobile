package com.nt.courierdriver.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.nt.courierdriver.data.model.DeliveryOrder;
import com.nt.courierdriver.databinding.ItemOrderListBinding;

import java.util.List;

public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.DeliveryOrderViewHolder> {

    private List<DeliveryOrder> mDeliveryOrderList;
    private IDeliveryOrderClickListener mIDeliveryOrderClickListener;

    public OrderRecyclerAdapter(IDeliveryOrderClickListener clickListener) {
        this.mIDeliveryOrderClickListener = clickListener;
    }

    public void setDeliveryOrderList(List<DeliveryOrder> orderList) {
        this.mDeliveryOrderList = orderList;
    }


    @NonNull
    @Override
    public DeliveryOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        //Use binding to inflate the recycler view item layout
        ItemOrderListBinding binding = ItemOrderListBinding.inflate(layoutInflater, parent, false);
        return new DeliveryOrderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryOrderViewHolder holder, int position) {
        DeliveryOrder order = mDeliveryOrderList.get(position);
        holder.bind(order);
    }

    @Override
    public int getItemCount() {
        return mDeliveryOrderList.size();
    }

    class DeliveryOrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemOrderListBinding mBinding;

        public DeliveryOrderViewHolder(ItemOrderListBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        public void bind(DeliveryOrder order) {
            //Sets the data variable being used in the item_order_list.xml
            mBinding.setDeliveryOrder(order);
            mBinding.executePendingBindings();

            //When view is clicked the onClick in this class will be called
            mBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            DeliveryOrder order = mDeliveryOrderList.get(getAdapterPosition());
            mIDeliveryOrderClickListener.onClick(order);
        }

    }
}
