package com.nt.courierdriver.ui.login;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String displayName;
    private String username;
    private String token;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(String displayName, String username, String token) {
        this.displayName = displayName;
        this.token = token;
        this.username = username;
    }

    String getDisplayName() {
        return displayName;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }
}