package com.nt.courierdriver.ui.order;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nt.courierdriver.OrderDetailActivity;
import com.nt.courierdriver.R;
import com.nt.courierdriver.data.ApiClient;
import com.nt.courierdriver.data.ApiInterface;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.data.model.DeliveryOrder;
import com.nt.courierdriver.data.model.DeliverySearchResponse;
import com.nt.courierdriver.ui.home.IDeliveryOrderClickListener;
import com.nt.courierdriver.ui.home.OrderRecyclerAdapter;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends Fragment implements IDeliveryOrderClickListener {

    private ApiInterface apiInterface;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_orders, container, false);
        fetchPendingOrders(root);
        return root;
    }

    private void fetchPendingOrders(View root) {

        final RecyclerView orderRecyclerView = root.findViewById(R.id.order_recycler_view);
        final LinearLayout noDeliveryLayout = root.findViewById(R.id.no_delivery_order);

        //Creates an instance of the products adapter and initialized the click listener with `this`
        //Meaning it will use the listener implemented in this class.
        final OrderRecyclerAdapter orderRecyclerAdapter = new OrderRecyclerAdapter(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        orderRecyclerView.setLayoutManager(layoutManager);
        orderRecyclerAdapter.setDeliveryOrderList(Collections.EMPTY_LIST);
        orderRecyclerView.setAdapter(orderRecyclerAdapter);
        //Sets the data that will be displayed on the UI

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            SharedPreferences pref = this.getActivity().getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String token = pref.getString(Constants.PREF_USER_TOKEN, "");
            Call<DeliverySearchResponse> call = apiInterface.doGetDeliveryList(0, 10, "",null,"Bearer "+token);
            call.enqueue(new Callback<DeliverySearchResponse>() {
                @Override
                public void onResponse(Call<DeliverySearchResponse> call, Response<DeliverySearchResponse> response) {
                    Log.d("HomeActivity",response.code()+"");
                    if(!response.isSuccessful()){
                        Log.d("HomeActivity",response.message()+"");
                        return;
                    }

                    DeliverySearchResponse searchResponse = response.body();
                    List<DeliveryOrder> orders = searchResponse.getResults();
                    orderRecyclerAdapter.setDeliveryOrderList(orders);
                    orderRecyclerView.setAdapter(orderRecyclerAdapter);
                    orderRecyclerView.setVisibility(View.VISIBLE);
                    noDeliveryLayout.setVisibility(View.GONE);
                    if(searchResponse.getTotal() == 0){
                        showToast(R.string.no_delivery_order);
                        orderRecyclerView.setVisibility(View.GONE);
                        noDeliveryLayout.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onFailure(Call<DeliverySearchResponse> call, Throwable t) {
                    Log.d("HomeActivity", "in error "+t.getLocalizedMessage());
                    Log.d("HomeActivity", "in error "+t.getCause().getMessage());
                    showToast(R.string.fetch_delivery_order_error);
                }
            });


        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(DeliveryOrder deliveryOrder) {
        Log.i("MainActivity", deliveryOrder.toString());
        Intent detailIntent = new Intent(getContext(), OrderDetailActivity.class);
        detailIntent.putExtra(Constants.Keys.DELIVERY_ORDER_DETAIL.toString(), deliveryOrder);
        startActivity(detailIntent);
    }

    private void showToast(@StringRes Integer errorString) {
        Toast.makeText(getContext(), errorString, Toast.LENGTH_LONG).show();
    }
}