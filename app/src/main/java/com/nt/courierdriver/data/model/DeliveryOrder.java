package com.nt.courierdriver.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DeliveryOrder implements Comparable<DeliveryOrder> , Parcelable {
    private String status;
    private String customerName;
    private String customerPhoneNo;
    private String customerEmail;
    private String recipientPhoneNo;
    private String recipientEmail;
    private String recipientName;
    private String deliveryNote;
    private Long estimatedAmount;
    private Long amount;
    private DeliveryAddress deliveryAddress;
    private DeliveryAddress pickUpAddress;
    private String deliveryItem;

    private Timestamp dateCreated;
    private User createdBy;
    private Driver driver;
    private String trackingId;
    private String paymentStatus;
    private Long amountPaid;
    private Timestamp datePaid;
    private String paymentTrxRef;
    private String formattedDateCreated;
    private String emptyString = "N/A";

    public DeliveryOrder() {
        status = "DELIVERED";
        customerName = "Nifemi Aina";
        customerPhoneNo = "09090909089";
        recipientName = "Funsho Idowu";
        recipientPhoneNo = "08078786767";
        dateCreated = new Timestamp(new Date().getTime());
        amount = 4000L;
        trackingId = "TR009";
        paymentStatus = "PAID";
        formattedDateCreated = getFormattedDateCreated();

    }

    protected DeliveryOrder(Parcel in) {
        status = in.readString();
        customerName = in.readString();
        customerPhoneNo = in.readString();
        customerEmail = in.readString();
        recipientPhoneNo = in.readString();
        recipientEmail = in.readString();
        recipientName = in.readString();
        deliveryNote = in.readString();
        if (in.readByte() == 0) {
            estimatedAmount = null;
        } else {
            estimatedAmount = in.readLong();
        }
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readLong();
        }
        deliveryItem = in.readString();
        trackingId = in.readString();
        paymentStatus = in.readString();
        if (in.readByte() == 0) {
            amountPaid = null;
        } else {
            amountPaid = in.readLong();
        }
        paymentTrxRef = in.readString();
        dateCreated = new Timestamp(in.readLong());
        pickUpAddress = in.readParcelable(DeliveryAddress.class.getClassLoader());
        deliveryAddress = in.readParcelable(DeliveryAddress.class.getClassLoader());
    }

    public static final Creator<DeliveryOrder> CREATOR = new Creator<DeliveryOrder>() {
        @Override
        public DeliveryOrder createFromParcel(Parcel in) {
            return new DeliveryOrder(in);
        }

        @Override
        public DeliveryOrder[] newArray(int size) {
            return new DeliveryOrder[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getRecipientPhoneNo() {
        return recipientPhoneNo;
    }

    public void setRecipientPhoneNo(String recipientPhoneNo) {
        this.recipientPhoneNo = recipientPhoneNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public Long getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Long estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public DeliveryAddress getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(DeliveryAddress pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveryItem() {
        return deliveryItem;
    }

    public void setDeliveryItem(String deliveryItem) {
        this.deliveryItem = deliveryItem;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Long getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Long amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Timestamp getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Timestamp datePaid) {
        this.datePaid = datePaid;
    }

    public String getPaymentTrxRef() {
        return paymentTrxRef;
    }

    public void setPaymentTrxRef(String paymentTrxRef) {
        this.paymentTrxRef = paymentTrxRef;
    }

    public String getFormattedDateCreated() {
        if(dateCreated == null){
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,yyyy hh:mm a");
        return formatter.format(dateCreated);
    }

    public String formatAmount(Long amount) {
        if(amount == null){
            return "";
        }
        return String.format("%,.2f", BigInteger.valueOf(amount).divide(BigInteger.valueOf(100L)).floatValue());
    }
    public String getEmptyString() {
        return emptyString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
        parcel.writeString(customerName);
        parcel.writeString(customerPhoneNo);
        parcel.writeString(customerEmail);
        parcel.writeString(recipientPhoneNo);
        parcel.writeString(recipientEmail);
        parcel.writeString(recipientName);
        parcel.writeString(deliveryNote);
        if (estimatedAmount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(estimatedAmount);
        }
        if (amount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(amount);
        }
        parcel.writeString(deliveryItem);
        parcel.writeString(trackingId);
        parcel.writeString(paymentStatus);
        if (amountPaid == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(amountPaid);
        }
        parcel.writeString(paymentTrxRef);
        parcel.writeLong(dateCreated.getTime());
        parcel.writeParcelable(pickUpAddress, i);
        parcel.writeParcelable(deliveryAddress, 1);
    }

    @Override
    public int compareTo(DeliveryOrder deliveryOrder) {
        if (trackingId.equals(deliveryOrder.trackingId)
                && customerName.equals(deliveryOrder.customerName)
                && recipientName == deliveryOrder.recipientName
                && amount == deliveryOrder.amount)
            return 0;
        return -1;
    }

    @Override
    public String toString() {
        return "DeliveryOrder{" +
                "status='" + status + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerPhoneNo='" + customerPhoneNo + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", recipientPhoneNo='" + recipientPhoneNo + '\'' +
                ", recipientEmail='" + recipientEmail + '\'' +
                ", recipientName='" + recipientName + '\'' +
                ", deliveryNote='" + deliveryNote + '\'' +
                ", estimatedAmount=" + estimatedAmount +
                ", amount=" + amount +
                ", deliveryAddress=" + deliveryAddress +
                ", pickUpAddress=" + pickUpAddress +
                ", deliveryItem='" + deliveryItem + '\'' +
                ", dateCreated=" + dateCreated +
                ", createdBy=" + createdBy +
                ", driver=" + driver +
                ", trackingId='" + trackingId + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", amountPaid=" + amountPaid +
                ", datePaid=" + datePaid +
                ", paymentTrxRef='" + paymentTrxRef + '\'' +
                ", formattedDateCreated='" + formattedDateCreated + '\'' +
                '}';
    }
}
