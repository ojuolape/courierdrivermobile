package com.nt.courierdriver.data;

import android.util.Log;

import com.nt.courierdriver.data.model.LoggedInUser;
import com.nt.courierdriver.data.model.LoginForm;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    ApiInterface apiInterface;

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoggedInUser> call = apiInterface.login(new LoginForm(username, password));
            call.enqueue(new Callback<LoggedInUser>() {
                @Override
                public void onResponse(Call<LoggedInUser> call, Response<LoggedInUser> response) {
                    Log.d("TAG",response.code()+"");
                    LoggedInUser loggedInUser = response.body();

                }

                @Override
                public void onFailure(Call<LoggedInUser> call, Throwable t) {
                    call.cancel();
                }
            });
            LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe", "098098");
            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}