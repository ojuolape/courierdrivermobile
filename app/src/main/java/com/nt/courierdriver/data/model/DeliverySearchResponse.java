package com.nt.courierdriver.data.model;

import java.util.List;

public class DeliverySearchResponse {
    List<DeliveryOrder> results;
    private int limit;
    private int pageNumber;
    private int total;

    public List<DeliveryOrder> getResults() {
        return results;
    }

    public int getLimit() {
        return limit;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getTotal() {
        return total;
    }
}
