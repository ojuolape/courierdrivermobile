package com.nt.courierdriver.data;

    import com.nt.courierdriver.data.model.DeliveryOrder;
    import com.nt.courierdriver.data.model.DeliverySearchResponse;
    import com.nt.courierdriver.data.model.DeliveryStatusUpdate;
    import com.nt.courierdriver.data.model.FcmToken;
    import com.nt.courierdriver.data.model.HomeStats;
    import com.nt.courierdriver.data.model.LoggedInUser;
    import com.nt.courierdriver.data.model.LoginForm;

    import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
    import retrofit2.http.Header;
    import retrofit2.http.POST;
    import retrofit2.http.Path;
    import retrofit2.http.Query;

public interface ApiInterface {

        @POST("/api/orders/{trackingId}/status")
        Call<DeliveryOrder> doUpdateDeliveryStatus(@Body DeliveryStatusUpdate user, @Path("trackingId") String trackingId, @Header("Authorization") String auth);

        @POST("/api/auth/driver/signin")
        Call<LoggedInUser> login(@Body LoginForm user);

        @GET("/api/orders?")
        Call<DeliverySearchResponse> doGetDeliveryList(@Query("pageNumber") int page, @Query("limit") int limit, @Query("q") String q, @Query("notDelivered") Boolean notDelivered, @Header("Authorization") String auth);

        @GET("/stats/admin/orders")
        Call<HomeStats> doGetStats(@Header("Authorization") String auth);

         @POST("/user/fcm-token")
         Call<LoggedInUser> doSendFcmToken(@Body FcmToken fcmToken,  @Header("Authorization") String auth);

    }

