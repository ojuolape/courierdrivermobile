package com.nt.courierdriver.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DeliveryAddress implements Parcelable {
    private String longitude;
    private String latitude;
    private String streetAddress;

    protected DeliveryAddress(Parcel in) {
        longitude = in.readString();
        latitude = in.readString();
        streetAddress = in.readString();
    }

    public static final Creator<DeliveryAddress> CREATOR = new Creator<DeliveryAddress>() {
        @Override
        public DeliveryAddress createFromParcel(Parcel in) {
            return new DeliveryAddress(in);
        }

        @Override
        public DeliveryAddress[] newArray(int size) {
            return new DeliveryAddress[size];
        }
    };

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(longitude);
        parcel.writeString(latitude);
        parcel.writeString(streetAddress);
    }
}
