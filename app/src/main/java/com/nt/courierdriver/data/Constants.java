package com.nt.courierdriver.data;

public class Constants {
    public static final String PREF_USER = "PREF_USER";
    public static final String PREF_USER_NAME = "USER_NAME";
    public static final String PREF_USER_TOKEN = "USER_TOKEN";
    public static final String PREF_USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    public static final String PREF_USER_PHONE_NUMBER = "PREF_USER_PHONE_NUMBER";
    public static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    public enum Keys {
        DELIVERY_ORDER_DETAIL
    }
}
