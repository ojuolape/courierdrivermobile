package com.nt.courierdriver.data.model;

public enum NotificationType {
    NEW_DELIVERY_REQUEST, DRIVER_ASSIGNED
}
