package com.nt.courierdriver.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.common.api.Api;
import com.nt.courierdriver.data.model.FcmToken;
import com.nt.courierdriver.data.model.LoggedInUser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationService {
    private static String TAG = "NotificationService";

    public static void sendRegistrationToServer(ApiInterface apiInterface, String token, Context context) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try{
            SharedPreferences pref = context.getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String userToken = pref.getString(Constants.PREF_USER_TOKEN, null);
            if(userToken == null){
                return;
            }
            Call<LoggedInUser> call = apiInterface.doSendFcmToken(new FcmToken(token), "Bearer "+userToken);
            call.enqueue(new Callback<LoggedInUser>() {
                @Override
                public void onResponse(Call<LoggedInUser> call, Response<LoggedInUser> response) {
                    Log.d(TAG,response.code()+"");
                }

                @Override
                public void onFailure(Call<LoggedInUser> call, Throwable t) {
                    call.cancel();
                }
            });


        } catch (Exception e) {

        }
    }
}
