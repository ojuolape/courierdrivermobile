package com.nt.courierdriver.data.model;

public class FcmToken {

    private String token;

    public FcmToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
