package com.nt.courierdriver.data.model;

public class DeliveryStatusUpdate {
    private String deliveryStatus;

    public DeliveryStatusUpdate(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
}
