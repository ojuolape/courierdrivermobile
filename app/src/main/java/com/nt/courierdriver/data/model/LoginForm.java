package com.nt.courierdriver.data.model;

public class LoginForm {

    private String username;
    private String password;
    private boolean rememberMe = true;

    public LoginForm(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }
}
