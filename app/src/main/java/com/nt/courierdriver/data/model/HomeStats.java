package com.nt.courierdriver.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigInteger;

public class HomeStats implements Parcelable {
    private long countToday;
    private long countThisMonth;
    private long totalToday;
    private long totalThisMonth;
    private int totalPendingDeliverires;

    protected HomeStats(Parcel in) {
        countToday = in.readLong();
        countThisMonth = in.readLong();
        totalToday = in.readLong();
        totalThisMonth = in.readLong();
        totalPendingDeliverires = in.readInt();
    }

    public static final Creator<HomeStats> CREATOR = new Creator<HomeStats>() {
        @Override
        public HomeStats createFromParcel(Parcel in) {
            return new HomeStats(in);
        }

        @Override
        public HomeStats[] newArray(int size) {
            return new HomeStats[size];
        }
    };

    public long getCountToday() {
        return countToday;
    }

    public void setCountToday(long countToday) {
        this.countToday = countToday;
    }

    public long getCountThisMonth() {
        return countThisMonth;
    }

    public void setCountThisMonth(long countThisMonth) {
        this.countThisMonth = countThisMonth;
    }

    public long getTotalToday() {
        return totalToday;
    }

    public void setTotalToday(long totalToday) {
        this.totalToday = totalToday;
    }

    public long getTotalThisMonth() {
        return totalThisMonth;
    }

    public void setTotalThisMonth(long totalThisMonth) {
        this.totalThisMonth = totalThisMonth;
    }

    public int getTotalPendingDeliverires() {
        return totalPendingDeliverires;
    }

    public void setTotalPendingDeliverires(int totalPendingDeliverires) {
        this.totalPendingDeliverires = totalPendingDeliverires;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(countToday);
        parcel.writeLong(countThisMonth);
        parcel.writeLong(totalToday);
        parcel.writeLong(totalThisMonth);
        parcel.writeInt(totalPendingDeliverires);
    }

    public String formatAmount(Long amount) {
        if(amount == null){
            return "";
        }
        return String.format("%,.2f", BigInteger.valueOf(amount).divide(BigInteger.valueOf(100L)).floatValue());
    }
}
