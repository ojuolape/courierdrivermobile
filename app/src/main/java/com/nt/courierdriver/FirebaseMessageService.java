package com.nt.courierdriver;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nt.courierdriver.data.ApiClient;
import com.nt.courierdriver.data.ApiInterface;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.data.NotificationService;
import com.nt.courierdriver.data.model.FcmToken;
import com.nt.courierdriver.data.model.LoggedInUser;
import com.nt.courierdriver.data.model.LoginForm;
import com.nt.courierdriver.data.model.NotificationType;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebaseMessageService extends FirebaseMessagingService {
    private static String TAG = "FMS";
    private ApiInterface mApiInterface;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

           if(remoteMessage.getData().get("notificationType").equalsIgnoreCase(NotificationType.DRIVER_ASSIGNED.toString())){
               Intent intent = new Intent(getBaseContext(), OrderAssignmentNotification.class);
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                   intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               }
               startActivity(intent);



           }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        NotificationService.sendRegistrationToServer(mApiInterface, token, getApplicationContext());
    }


}
