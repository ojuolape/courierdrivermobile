package com.nt.courierdriver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.firebase.FirebaseApp;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
            SharedPreferences pref = getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String token = pref.getString(Constants.PREF_USER_TOKEN, null);

            if(token == null){
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                return;
            }else {
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }



    }
}