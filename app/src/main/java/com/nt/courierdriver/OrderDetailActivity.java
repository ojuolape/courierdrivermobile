package com.nt.courierdriver;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.nt.courierdriver.data.ApiClient;
import com.nt.courierdriver.data.ApiInterface;
import com.nt.courierdriver.data.Constants;
import com.nt.courierdriver.data.model.DeliveryOrder;
import com.nt.courierdriver.data.model.DeliverySearchResponse;
import com.nt.courierdriver.data.model.DeliveryStatusUpdate;
import com.nt.courierdriver.databinding.ActivityOrderDetailBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends AppCompatActivity {
    private ApiInterface mApiInterface;
    private DeliveryOrder mDeliveryOrder;
    private ActivityOrderDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent detailIntent = getIntent();
        Bundle extras = detailIntent.getExtras();

        if (extras == null) return;

        //Initialize binding object by passing in the activity of interest `this` and layout id
        //and with that you dont need to use findViewById to set values for the UI.
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);

        mDeliveryOrder = (DeliveryOrder) extras.getParcelable(Constants.Keys.DELIVERY_ORDER_DETAIL.toString());

        if (mDeliveryOrder == null) return;

        mBinding.setDeliveryOrder(mDeliveryOrder);
        Button callSenderButton = findViewById(R.id.call_sender);
        callSenderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNumber(mDeliveryOrder.getCustomerPhoneNo());
            }
        });
        Button calRecipientButton = findViewById(R.id.call_recipient);
        calRecipientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNumber(mDeliveryOrder.getRecipientPhoneNo());
            }
        });

    }

    private void callNumber(String s) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+s));//change the number
        startActivity(callIntent);
    }

    private void showToast(@StringRes Integer errorString) {
        Toast.makeText(this, errorString, Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_delivered) {
            updateDeliveryStatus("DELIVERED");
            invalidateOptionsMenu();
            return true;
        } else if(id == R.id.menu_picked_up){
            updateDeliveryStatus("IN_TRANSIT");
            invalidateOptionsMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateDeliveryStatus(String status) {
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            SharedPreferences pref = getSharedPreferences(Constants.PREF_USER, Context.MODE_PRIVATE);
            String token = pref.getString(Constants.PREF_USER_TOKEN, "");
            Call<DeliveryOrder> call = mApiInterface.doUpdateDeliveryStatus(new DeliveryStatusUpdate(status), mDeliveryOrder.getTrackingId(),"Bearer "+token);
            call.enqueue(new Callback<DeliveryOrder>() {
                @Override
                public void onResponse(Call<DeliveryOrder> call, Response<DeliveryOrder> response) {
                    Log.d("OrderDetailActivity",response.code()+"");
                    if(!response.isSuccessful()){
                        Log.d("OrderDetailActivity",response.message()+"");
                        showToast(R.string.error_deliveyr_status_update);
                        return;
                    }

                    mDeliveryOrder = response.body();
                    if(mDeliveryOrder != null){
                        showToast(R.string.success_delivery_status_update);
                        mBinding.setDeliveryOrder(mDeliveryOrder);
                        //mBinding.executePendingBindings();
                    }

                }

                @Override
                public void onFailure(Call<DeliveryOrder> call, Throwable t) {
                    showToast(R.string.error_deliveyr_status_update);
                }
            });


        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //disable next menu if note item is the last in the array. The disable will work after you call the invalidateOptionsMenu()
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem deliverMenuItem = menu.findItem(R.id.menu_delivered);
        MenuItem pickUpMenuItem = menu.findItem(R.id.menu_picked_up);
        deliverMenuItem.setVisible(mDeliveryOrder.getStatus().equalsIgnoreCase("DRIVER_ASSIGNED") || mDeliveryOrder.getStatus().equalsIgnoreCase("IN_TRANSIT"));
        pickUpMenuItem.setVisible(mDeliveryOrder.getStatus().equalsIgnoreCase("DRIVER_ASSIGNED"));
        return super.onPrepareOptionsMenu(menu);
    }
}